# 0.0.1

* Support `trifecta-1.6`

# 0.0

* Split `indentation` into separate `indentation-core`, `indentation-parsec` and `indentation-trifecta` packages.
  Keep the original `indentation` for backward compatability and as a general roll-up package.
